﻿/// <reference path="../angular/angular.js" />
/// <reference path="../angular/module.js" />
/// <reference path="../dx.all.debug.js" />
(function() {
    app.controller("IndexCtrl",
        [
            "$scope", "$http", function($scope, $http) {
                $scope.dataGridOptions = {
                    dataSource: new DevExpress.data.DataSource({
                        key: "question_id",
                        load: function() {
                            return $.getJSON("/Home/GetData/");
                        },
                        map: function(itemData) {
                            return {
                                answer_count: itemData.answer_counts,
                                creation_date: new Date(itemData.creation_date * 1000),
                                last_activity_date: new Date(itemData.last_activity_date * 1000),
                                is_answered: itemData.is_answered,
                                link: itemData.link,
                                owner: itemData.owner,
                                question_id: itemData.question_id,
                                score: itemData.score,
                                tags: itemData.tags,
                                title: itemData.title,
                                view_count: itemData.view_count,
                            }
                        }
                    }),
                    columns: [
                        {
                            dataField: "link",
                            caption: "Link",
                            cellTemplate: function(container, options) {
                                $("<a href='" + options.data.link + "' target='_blank' />").text(options.data.title)
                                    .appendTo(container);
                            }
                        },
                        {
                            dataField: "answer_count",
                            caption: "Answer Count"
                        },
                        {
                            dataField: "view_count",
                            caption: "View Count",
                            alignment: "center"
                        },
                        {
                            dataField: "creation_date",
                            caption: "Creation Date",
                            alignment: "center"
                        },
                        {
                            dataField: "is_answered",
                            caption: "Is Answered",
                            alignment: "center"
                        }
                    ],
                    masterDetail: {
                        enabled: true,
                        template: "detail"
                    },
                    headerFilter: {
                        allowSearch: true,
                        visible: true
                    },
                    loadPanel: {
                        enabled: "auto",
                        showIndicator: true,
                        showPane: true
                    },
                    searchPanel: { visible: true },
                    selection: { mode: "single" },
                    columnAutoWidth: true,
                    scrolling: {
                        useNative: true,
                        scrollByContent: true,
                        showScrollbar: "onHover",
                        mode: "standard", // or "virtual"
                        direction: "both"
                    },
                    groupPanel: { visible: true },
                    paging: { enabled: true, pageSize: 10 },
                    pager: {
                        allowedPageSizes: "auto",
                        showInfo: true,
                        showNavigationButtons: true,
                        showPageSizeSelector: true,
                        visible: "auto"
                    },
                    showColumnLines: true,
                    showBorders: true,
                    showRowLines: true,
                    allowColumnResizing: true,
                    columnResizingMode: "widget",
                    allowColumnReordering: true,
                    filterRow: { visible: true },
                    onInitialized: function(e) {
                        $scope.dataGridInstance = e.component;
                    },
                    onToolbarPreparing: function(e) {
                        var dataGrid = e.component;
                        e.toolbarOptions.items.unshift(
                            {
                                location: "after",
                                widget: "dxButton",
                                options: {
                                    icon: "refresh",
                                    hint: "Refresh",
                                    onClick: function() {
                                        dataGrid.refresh();
                                    }
                                }
                            });
                    }
                };
            }
        ]);
})();