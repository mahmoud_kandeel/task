using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Task.Models;

namespace Task.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult GetData()
        {
            using (var handler = new HttpClientHandler())
            {
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (var client = new HttpClient(handler, false))
                {
                    var url = new Uri("https://api.stackexchange.com/2.2/questions");
                    var urlParameters = "?order=desc&pagesize=50&sort=creation&site=stackoverflow";
                    client.BaseAddress = url;
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(urlParameters).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var searchResult = response.Content.ReadAsAsync<SearchResult>().Result;
                        return Json(searchResult.items, JsonRequestBehavior.AllowGet);
                    }

                    Console.WriteLine("{0} ({1})", (int) response.StatusCode, response.ReasonPhrase);
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}